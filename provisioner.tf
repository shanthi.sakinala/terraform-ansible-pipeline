resource "null_resource" "provisioner" {

  # This trigger iterates every time the item is run, causing the resource to be changed, requiring an update.
  triggers = {
    build_number = "${timestamp()}"
  }

  provisioner "local-exec" {
    command = "echo ${aws_eip.ip_address.public_ip} > ./inventory" # Create ./inventory file from public IP address to be consumed by Ansible
  }

  provisioner "remote-exec" {
    inline = [
      # "sudo ${var.linux_package_manager} update",
      # "sudo ${var.linux_package_manager} upgrade -y",
      "sudo ${var.linux_package_manager} install python3 -y"
    ] # Upgrade instance and install Python for use by Ansible

    connection {
      type = "ssh"
      user = var.remote_ssh_user # The remote SSH user
      #private_key = file(var.key_location) # The private key location on local machine
      host = aws_eip.ip_address.public_ip
    }
  }
}
