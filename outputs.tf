#outputs

output "ip_address" {
  description = "External IP address of Elastic IP (Instance)"
  value       = aws_eip.ip_address.public_ip
}

output "name" {
  description = "Name of instance"
  value       = var.name.Name
}
